/// <reference types="react" />
import * as msal from "@azure/msal-browser";
export declare const createMsalInstance: ({ clientId, tenantId, redirectUri }: {
    clientId?: string | undefined;
    tenantId?: string | undefined;
    redirectUri?: string | undefined;
}) => msal.PublicClientApplication;
export declare const TokenComponent: (props: any) => JSX.Element;
