import React, { useState, useEffect } from 'react';
import * as msal from "@azure/msal-browser";
export const createMsalInstance = ({ clientId = "", tenantId = "", redirectUri = '' }) => {
    const msalConfig = {
        auth: {
            clientId: clientId,
            authority: `https://login.microsoftonline.com/${tenantId}`,
            redirectUri
        }
    };
    const msalInstance = new msal.PublicClientApplication(msalConfig);
    return msalInstance;
};
export const TokenComponent = (props) => {
    const { msalInstance } = props;
    const [token, setToken] = useState();
    const localToken = window.localStorage.getItem('AccessToken');
    if (!token && localToken) {
        const tkn = window.localStorage.getItem('AccessToken') || '';
        setToken(tkn);
    }
    useEffect(() => {
        if (!token) {
            function handleResponse(resp) {
                let accountId;
                if (resp !== null) {
                    accountId = resp.account.homeAccountId;
                    //console.log(resp);
                    window.localStorage.setItem('AccessToken', resp.accessToken);
                    setToken(resp.accessToken);
                }
                else {
                    // need to call getAccount here?
                    const currentAccounts = msalInstance.getAllAccounts();
                    if (!currentAccounts || currentAccounts.length < 1) {
                        return;
                    }
                    else if (currentAccounts.length > 1) {
                        // Add choose account code here
                    }
                    else if (currentAccounts.length === 1) {
                        accountId = currentAccounts[0].homeAccountId;
                        //console.log(currentAccounts[0]);
                        //setToken(currentAccounts[0].accessToken)
                    }
                }
            }
            msalInstance.handleRedirectPromise().then(handleResponse).catch((err) => {
                console.error(err);
            });
        }
    });
    const signIn = () => {
        var loginRequest = {
            scopes: ["user.read"] // optional Array<string>            
        };
        try {
            msalInstance.loginRedirect(loginRequest);
        }
        catch (err) {
            // handle error
        }
    };
    return token ?
        (React.createElement("div", null, "Logged In")) :
        (React.createElement("div", null,
            React.createElement("button", { onClick: signIn }, "Log In")));
};
//# sourceMappingURL=index.js.map