import React from 'react';
import { render} from 'react-dom';

import { TokenComponent } from '../../src';

const App = () => (
    <div>
        <h1>msal-token-lib Demo</h1>
            <TokenComponent/>
    </div>
);
render(<App />, document.querySelector('#demo'));

