"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validate = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const jwks_rsa_1 = __importDefault(require("jwks-rsa"));
var client = jwks_rsa_1.default({
    jwksUri: 'https://login.windows.net/common/discovery/keys'
});
function getKey(header, callback) {
    client.getSigningKey(header.kid, function (err, key) {
        var signingKey = key.getPublicKey();
        console.log(signingKey);
        callback(null, signingKey);
    });
}
const validate = (authHeader) => {
    return new Promise((resp) => {
        let accessToken = authHeader.replace('Bearer ', '');
        jsonwebtoken_1.default.verify(accessToken.toString(), getKey, {}, function (err, decoded) {
            if (false /*|| err*/) { //Todo 
                resp({
                    authorized: false,
                    message: err
                });
            }
            else {
                resp({
                    authorized: true,
                    token: jsonwebtoken_1.default.decode(accessToken)
                });
            }
        });
    });
};
exports.validate = validate;
//# sourceMappingURL=index.js.map