import jwt  from 'jsonwebtoken'
import jwksClient from 'jwks-rsa'

var client = jwksClient({
  jwksUri: 'https://login.windows.net/common/discovery/keys'
});

function getKey(header:any, callback:any){
  client.getSigningKey(header.kid, function(err:any, key:any) {
    var signingKey = key.getPublicKey();
    console.log(signingKey)    
    callback(null, signingKey);
  });
}

export const validate = (authHeader: string)=>{
  return new Promise((resp)=>{
    let accessToken = authHeader.replace('Bearer ','')
    jwt.verify(accessToken.toString(), getKey, {}, function(err:any, decoded:any) {
      if(false /*|| err*/){//Todo 
        resp({
          authorized: false,
          message: err
        })
      }else {
        resp({
          authorized: true,
          token: jwt.decode(accessToken)
        })
      }
    })
  })
}
