import Debug from 'debug'
import { AzureFunction, Context, HttpRequest } from "@azure/functions"
//import {validate}  from '../src/auth'
import {validate}  from 'msal-token-server'
const debug = Debug('ModelService')

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    debug('ModelService function processing a request.');

    const authResult: any = await validate(req.headers.authorization)    
    if(!authResult.authorized){
        context.res = {
            status: 400,
            body: authResult.message
        };
    }else if(req.method==='POST'){        
        context.res = {
            status: 200,
            body: "Post Handled"
        };
    }else if(req.method==='GET'){    

        context.res = {
            status: 200,
            body: [{ status: "Success"}]
        };
    }
};

export default httpTrigger;