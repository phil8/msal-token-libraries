const DOTENV = require('dotenv')
const DOTENV_CONFIG = process.env.ENVIRONMENT ? `.env.${process.env.ENVIRONMENT}` : '.env'
console.log(`Sourcing configuration from ${DOTENV_CONFIG}`)
DOTENV.config({ path: DOTENV_CONFIG })

console.log(`Debugging with ${process.env.DEBUG}`)

const LOCAL = process.env.NODE_ENV !== 'production'
const SERVER_PORT = process.env.PORT || 3001

const database = {
  USER: process.env.DB_USER,
  PASSWORD: process.env.DB_PASSWORD,
  DBHOST: process.env.DB_SERVER,
  DB: process.env.DB_DATABASE,
  DBDIALECT: 'sqlite',
  force: true,
  RESYNC: 'true',
  logging: true,
}

const AUTH0_AUDIENCE = 'http://aureconalign.aurecongroup.digital'
const AUTH0_ROLES = 'https://aurecongroup.com/roles'
const AUTH0_EMAIL = 'https://aurecon.com/email'
const AUTH0_METADATA = 'https://aurecongroup.com/user_metadata'
const AUTH0_GLOBAL_ADMIN_ROLE = 'aureconAlign Global Admin'
const AUTH0_GLOBAL_ADMIN_ROLE_ID = 'rol_BTIs4F8XGYkMnSJm'
const AUTH0_DOMAIN = 'https://creativetechnologies.au.auth0.com'
const AUTH0_CLIENT_ID = 'P61pY9xsYzLKBLZzeiNYFuHMgzeMtmxM'
const AUTH0_CLIENT_SECRET = process.env.CLIENT_SECRET
const AUTH0_API_URL = 'https://creativetechnologies.au.auth0.com/api/v2'
const SENDGRID_API_KEY = process.env.SENDGRID_API_KEY
const APP_URL = process.env.APP_URL
const STORAGE_DIRECTORY = '/tmp/'

export const config =  {
  LOCAL,
  SERVER_PORT,
  AUTH0_AUDIENCE,
  AUTH0_ROLES,
  AUTH0_EMAIL,
  AUTH0_METADATA,
  AUTH0_GLOBAL_ADMIN_ROLE,
  AUTH0_GLOBAL_ADMIN_ROLE_ID,
  AUTH0_DOMAIN,
  AUTH0_CLIENT_ID,
  AUTH0_CLIENT_SECRET,
  AUTH0_API_URL,
  SENDGRID_API_KEY,
  STORAGE_DIRECTORY,
  APP_URL,
  database,
  seed: true, //Seed the database
  testMode: false, // use this to turn off authentication  
}
