///This is not used. The component used comes from the library

import React, { useEffect, useMemo, useState }  from 'react';
import * as msal from "@azure/msal-browser";

const REACT_APP_TENANT_CLIENT_ID='c8364291-dd11-46fa-99c6-1af7935e4806'
const REACT_APP_TENANT_ID='951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d'

const msalConfig : any = {
    auth: {
        clientId: REACT_APP_TENANT_CLIENT_ID,
        authority: `https://login.microsoftonline.com/${REACT_APP_TENANT_ID}`,
        redirectUri: 'http://localhost:3000'
    }
};

const msalInstance = new msal.PublicClientApplication(msalConfig);

export const Token = (props:any) => {

    const [token,setToken] = useState<string>()

    const localToken = window.localStorage.getItem('AccessToken')

    if(!token && localToken){
        const tkn = window.localStorage.getItem('AccessToken') || ''
        setToken (tkn)
    }

    useEffect(()=>{
        if(!token){
            function handleResponse(resp:any) {
                let accountId
                if (resp !== null) {
                    accountId = resp.account.homeAccountId;
                    //console.log(resp);
                    window.localStorage.setItem('AccessToken',resp.accessToken)
                    setToken(resp.accessToken)
                } else {
                    // need to call getAccount here?
                    const currentAccounts = msalInstance.getAllAccounts();
                    if (!currentAccounts || currentAccounts.length < 1) {
                        return;
                    } else if (currentAccounts.length > 1) {
                        // Add choose account code here
                    } else if (currentAccounts.length === 1) {
                        accountId = currentAccounts[0].homeAccountId;
                        //console.log(currentAccounts[0]);
                        //setToken(currentAccounts[0].accessToken)
                    }
                }
            }

            msalInstance.handleRedirectPromise().then(handleResponse).catch((err:string) => {
                console.error(err);
            });
        
        }
    })
    
    const signIn = () => {
        var loginRequest = {
            scopes: ["user.read"] // optional Array<string>            
        };
        
        try {
            msalInstance.loginRedirect(loginRequest);
        } catch (err) {
            // handle error
        }
    }
      
    return token ? 
        (<div>Logged In</div>) : 
        (<div>        
            <button onClick={signIn}>Log In</button>
        </div>)
}