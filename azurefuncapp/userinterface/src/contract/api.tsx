import { getAPI, getAPIDomain } from '../config';

const handleResponse = (res:Response) => {
  if (res.status === 401) {
    window.location.href = `${getAPIDomain()}/auth/login`
  } else if (res.status === 500) {
      return null
  } else {
    return (res!== null) ? res.json() : []
  }
}

const service = {
  get: (url:string,options:object) => fetch(getAPI() + url, { ...options,/*credentials: 'include'*/ }).then(handleResponse),
  post: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'POST', body: JSON.stringify(body) }).then(handleResponse),
  put: (url:string, body:object,options:object={}) => fetch(getAPI() + url, { ...options,/*credentials: 'include',*/ method: 'PUT', body: JSON.stringify(body) }).then(handleResponse),
  delete: (url:string,options:object={}) => fetch(getAPI() + url, {...options, /*credentials: 'include',*/ method: 'DELETE' }).then(handleResponse)
}

export async function createDataset(data:object) {
  return service.post("/DataSetService", data)
}

export async function createModelConfiguration(data:object) {  
  const localToken = window.localStorage.getItem('AccessToken')
  return service.post("/ModelService/1", data,{headers: {
    'Authorization': `Bearer ${localToken}`
  }})
}
export async function queryModelComponents(data:string) {

  const localToken = window.localStorage.getItem('AccessToken')

  return service.get(`/ModelService/${data}`,{headers: {
    'Authorization': `Bearer ${localToken}`
  }})
}

// export async function queryProjectFiles(projectId: string) {
//   return service.get(`/projects/${projectId}/files`)
// }