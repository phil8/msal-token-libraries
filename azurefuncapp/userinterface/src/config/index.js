
let location = window.location.origin

//let port = process.env.REACT_APP_BACKEND_PORT || 3000

export function getAPIDomain() {
    if(location.indexOf('localhost')>=0)
        return "http://localhost:7071"
    else
        return "https://fu-gpc-funapp-dev.azurewebsites.net"
}

// export function getSocketUrl() {
//     return getAPIDomain().replace(':3000',`:${port}`)
// }

export function getAPI() {
    let domain = getAPIDomain()
    return `${domain}/api`
}
