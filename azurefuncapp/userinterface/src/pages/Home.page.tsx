import React, { FC } from 'react';
import Style from '../styles/Page.module.sass';

const Home: FC = () => {

    return <div className={Style.pageContainer}>            
            <div className={Style.pageTitle}>Home</div>
            <div className={Style.pageContent}>
                Some intro
            </div>
    </div>
}

export default Home