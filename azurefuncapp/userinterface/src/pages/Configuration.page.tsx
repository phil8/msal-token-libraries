import React, { FC, useEffect, useState } from 'react';
import { queryModelComponents, createModelConfiguration  } from '../contract/api';
import Style from '../styles/Page.module.sass';

export const Component = (props:any) => {
    const {data} = props
    return <div key={data.id} >{JSON.stringify(data)}</div>
}

const Configuration: FC = () => {

    const [components,setComponents] = useState<any>([])

    useEffect(()=>{
        async function getData(){
            const comps = await queryModelComponents('3')
            setComponents(comps)
        }
        getData()
    },[])

    const handleButton = ()=>{        
        createModelConfiguration({})
    }

    return <div className={Style.pageContainer}>
        <div className={Style.pageTitle}>Configuration</div>
        <div className={Style.pageContent}>
            {components.map((comp:any)=><Component key={comp.id} data={comp}/>)}
        </div>

        <button onClick={handleButton}>Post</button> 
    </div>
}

export default Configuration