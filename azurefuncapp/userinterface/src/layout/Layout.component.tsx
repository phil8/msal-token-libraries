import React from 'react'
import Style from '../styles/Layout.module.sass';
//import {Token} from '../components/Token.component'
import { createMsalInstance, TokenComponent} from 'msal-token-lib'

const REACT_APP_TENANT_CLIENT_ID='c8364291-dd11-46fa-99c6-1af7935e4806'
const REACT_APP_TENANT_ID='951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d'

const msalInstance = createMsalInstance({
  clientId: REACT_APP_TENANT_CLIENT_ID,
  tenantId: REACT_APP_TENANT_ID,
  redirectUri: 'http://localhost:3000'
})

export const Menu = (props: any) => {
  return <div className={Style.menu}>Menu |  <a href="#/configuration">Configuration</a>| </div>
}

export const Layout = (props: any) => {
  return <div className={Style.layoutContainer}>
      <div className={Style.header}>
        Header
        <TokenComponent msalInstance={msalInstance}/>
      </div>
      
      <Menu/>   
      {props.children}
      <div className={Style.footer}>Footer</div>
    </div>
}
