import React, { Suspense } from 'react';
import { HashRouter, Switch, Route } from "react-router-dom"
import { Layout } from './layout'
import Home from './pages/Home.page'
import Configuration from './pages/Configuration.page'
import { Loader } from './components'

function App() {
  return <Suspense fallback={<Loader />}>
          <HashRouter hashType='slash'>
            <Layout>
              <Switch>                
                <Route path="/configuration" component={Configuration} />
                <Route path="/" exact component={Home} />
              </Switch>    
              </Layout>
            </HashRouter>
          </Suspense>
}

export default App; 