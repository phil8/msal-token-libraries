# azurefuncapp

This templates provides a 
- react webpack user interface web application
- an azure function app services
- configuruation to integrate with a sql service database
- sqlite in memory db
- a shared application library

The language is typescript.

It is being developed as part of the Technical Excellence BST code.


###  Directory Structure

The repository contains 2 deployment units and 1 common application library

This components are to be assembled in the following way

![](./content/images/ModelSpecifocationApplicationComponents.png)


- The React application runs on the browser. More [here](./userinterface/README.md)
- The Azure Functions will run in the cloud. More [here](./services/README.md)
- The application library is intended to be shared functionality which could either run in the browser or on the azure functions.

### Git Flow

This repository is initialized for use with [Gitflow extension to git](https://github.com/petervanderdoes/gitflow-avh/wiki)

### Node

This is a nodejs based application.
Please install node with [nvm](https://github.com/nvm-sh/nvm)

### React

`create-react-app` was used to create this single page application but this was ejected to expose the webpack configuration.

### Typescript

The react application and the azure functions are set up for typescript.

### Azure Functions

This project deploys service components as azure functions.

For development please ensure that [azure core functions](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local) is installed.

New functions have been created with

```
func new
```

## Developing

Open vscode at the root of the repository where you can see userinterface, service and application sub folders

```
npm start
```

Will start
- the webpack build process
- an http server serving the dist directory
- run `func start`  to run the azure function library


### Alternative you can do these individually

Start the webpack dev server with
```
cd userinterface
npm start // to run the webpack build process
npm run service // to run the http server
```

Start the azure function runtime with

```
cd services
npm start
```

Start the application development

```
cd application
npm start
```

### Styling

Define a sass file under userinterface/styles, then use those styles in the react component
```
import Style from '../styles/Page.module.sass';
....
   return <div className={Style.pageContainer}>....</div>
```

## Deployment

The application is deployed to the cloud as a Dockerfile via gitHub actions.
