This is some code to figure out how to capture an access token and validate it within an azure function.

The azurefuncapp is a web app with react front and and azure func app back end which is being used to check if the library components can be used in an typescript application.

The msal-token-lib has a typescript react component which can be used to acquire a token.

The msal-token-server has a typescript component which exposes a function to validate a token.

***Todo-The signature validation is not working for some reason***

[How to verify the token](https://blogs.aaddevsup.xyz/2019/03/using-jwt-io-to-verify-the-signature-of-a-jwt-token/)